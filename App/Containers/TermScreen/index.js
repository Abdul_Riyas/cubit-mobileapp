import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'native-base';
class TermScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Terms and Conditons',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />
    }
  }

  componentDidMount() {
    this.props.navigation.addListener('focus',
      payload => {
        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor(COLOR.primary1);
      });
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.scrollbox}>
        <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
        <View style={styles.box1}>
          <Text style={styles.text}>Welcome to Cubit!</Text>
          <Text style={styles.text1}>
            These terms and conditions outline the rules and
            regulations for the use of Cubit's Application,
            located at www.Cubit-talks.co.in.
             </Text>
          <Text style={styles.text1}>
            By accessing this Application we assume you accept these
            terms and conditions. Do not continue to use Cubit
            if you do not agree to take all of the terms and
            conditions stated on this page. Our Terms and
            Conditions were created with the help of the Terms
            And Conditions Generator and the Free Terms & Conditions Generator.
             </Text>
          <Text style={styles.text1}>
            The following terminology applies to these Terms and Conditions,
            Privacy Statement and Disclaimer Notice and all
            Agreements: "Client", "You" and "Your" refers to you,
            the person log on this Application and compliant to the Company’s
            terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company.
            "Party", "Parties", or "Us", refers to both the Client and ourselves.
            All terms refer to the offer, acceptance and consideration of
            payment necessary to undertake the process of our assistance
            to the Client in the most appropriate manner for the express
            purpose of meeting the Client’s needs in respect of provision
            of the Company’s stated services, in accordance with and
            subject to, prevailing law of Netherlands. Any use of the
            above terminology or other words in the singular, plural,
            capitalization and/or he/she or they, are taken as interchangeable
            and therefore as referring to same.
             </Text>
          <Text style={styles.text}>Cookies</Text>
          <Text style={styles.text1}>
            We employ the use of cookies.
            By accessing Cubit, you agreed to use
            cookies in agreement with the Cubit's Privacy Policy.
             </Text>
          <Text style={styles.text1}>
            Most interactive Applications use cookies to let us retrieve the
            user’s details for each visit. Cookies are used by our Application to enable the
            functionality of certain areas to make it easier for people visiting
            our Application. Some of our affiliate/advertising partners may also use cookies.
             </Text>
          <Text style={styles.text}>License</Text>
          <Text style={styles.text1}>
            Unless otherwise stated, Cubit and/or its licensors own
            the intellectual property rights for all material on Cubit. All intellectual property
            rights are reserved. You may access this from Cubit for
            your own personal use subjected to restrictions set in these terms and conditions.
             </Text>
          <Text style={styles.text}>You must not:</Text>
          <Text style={styles.text1}>Republish material from Cubit</Text>
          <Text style={styles.text1}>Sell, rent or sub-license material from Cubit</Text>
          <Text style={styles.text1}>Reproduce, duplicate or copy material from Cubit</Text>
          <Text style={styles.text1}>Redistribute content from Cubit</Text>
          <Text style={styles.text1}>This Agreement shall begin on the date hereof.</Text>
          <Text style={styles.text1}>
            Parts of this Application offer an opportunity for users to post and
            exchange opinions and information in certain areas of the Application. Cubit
            Talks does not filter, edit, publish or review Comments prior to
            their presence on the Application. Comments do not reflect the views and
            opinions of Cubit,its agents and/or affiliates. Comments reflect
            the views and opinions of the person who post their views and opinions.
            To the extent permitted by applicable laws, Cubit shall not be liable
            for the Comments or for any liability, damages or
            expenses caused and/or suffered as a result of any use of and/or posting
            of and/or appearance of the Comments on this Application.
             </Text>
          <Text style={styles.text1}>
            Cubit reserves the right to monitor all Comments and to remove any
            Comments which can be considered inappropriate, offensive or causes breach
            of these Terms and Conditions.
             </Text>
          <Text style={styles.text}>You warrant and represent that:</Text>
          <Text style={styles.text1}>
            You are entitled to post the Comments on our Application and have all
            necessary licenses and consents to do so;
            The Comments do not invade any intellectual property right, including without
            limitation copyright, patent or trademark of any third party;
             </Text>
          <Text style={styles.text1}>
            The Comments do not contain any defamatory, libelous, offensive,
            indecent or otherwise unlawful material which is an invasion of privacy
             </Text>
          <Text style={styles.text1}>
            The Comments will not be used to solicit or promote business or
            custom or present commercial activities or unlawful activity.
             </Text>
          <Text style={styles.text1}>
            You hereby grant Cubit a non-exclusive license to use, reproduce,
            edit and authorize others to use, reproduce and edit any of your Comments
            in any and all forms, formats or media.
             </Text>
          <Text style={styles.text}>
            Hyperlinking to our Content
            The following organizations may link to our Application without prior written approval:
             </Text>
          <Text style={styles.text}>
            Government agencies;
          </Text>
          <Text style={styles.text}>
            Search engines;
          </Text>
          <Text style={styles.text}>
            News organizations;
          </Text>
          <Text style={styles.text1}>
            Online directory distributors may link to our Application in the same manner as they hyperlink to the Applications of other listed businesses; and
            System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.
          </Text>
          <Text style={styles.text1}>
            These organizations may link to our home page, to publications or to other Application information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.
          </Text>
          <Text style={styles.text}>
            We may consider and approve other link requests from the following types of organizations:
          </Text>
          <Text style={styles.text}>
            commonly-known consumer and/or business information sources;
            dot.com community sites;
          </Text>
          <Text style={styles.text}>
            associations or other groups representing charities;
          </Text>
          <Text style={styles.text}>
            online directory distributors;
          </Text>
          <Text style={styles.text}>
            internet portals;
          </Text>
          <Text style={styles.text}>
            accounting, law and consulting firms; and
            educational institutions and trade associations.
          </Text>
          <Text style={styles.text1}>
            We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from
            the visibility of the hyperlink compensates the absence of Cubit; and (d) the link is in the context of general resource information.
          </Text>
          <Text style={styles.text1}>
            These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship,
            endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.
          </Text>
          <Text style={styles.text1}>
            If you are one of the organizations listed in paragraph 2 above and are interested in linking to our Application, you must inform us by sending an e-mail to Cubit. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you
            intend to link to our Application, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.
          </Text>
          <Text style={styles.text}>
            Approved organizations may hyperlink to our Application as follows:
          </Text>
          <Text style={styles.text1}>
            By use of our corporate name; or
            By use of the uniform resource locator being linked to; or
            By use of any other description of our Application being linked to that makes sense within the context and format of content on the linking party’s site.
            No use of Cubit's logo or other artwork will be allowed for linking absent a trademark license agreement.
          </Text>
          <Text style={styles.text}>
            iFrames
          </Text>
          <Text style={styles.text1}>
            Without prior approval and written permission, you may not create frames around our
            Webpages that alter in any way the visual presentation or appearance of our Application.
          </Text>
          <Text style={styles.text}>
            Content Liability
          </Text>
          <Text style={styles.text1}>
            We shall not be hold responsible for any content that appears on your Application. You agree to protect and defend us against all claims that is rising on your Application. No link(s) should appear on any Application that may be interpreted as libelous,
            obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.
          </Text>
          <Text style={styles.text}>
            Your Privacy
          </Text>
          <Text style={styles.text}>
            Please read Privacy Policy
          </Text>
          <Text style={styles.text}>
            Reservation of Rights
          </Text>
          <Text style={styles.text1}>
            We reserve the right to request that you remove all links or any particular link to our Application. You approve to immediately remove all links to our Application upon request. We also reserve the right to amen these terms and conditions and it’s linking policy
            at any time. By continuously linking to our Application, you agree to be bound to and follow these linking terms and conditions.
          </Text>
          <Text style={styles.text}>
            Removal of links from our Application
          </Text>
          <Text style={styles.text1}>
            If you find any link on our Application that is offensive for any reason, you are free to contact
            and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.
          </Text>
          <Text style={styles.text1}>
            We do not ensure that the information on this Application is correct, we do not warrant its completeness or
            accuracy; nor do we promise to ensure that the Application remains available or that the material on the Application is kept up to date.
          </Text>
          <Text style={styles.text}>
            Disclaimer
          </Text>
          <Text style={styles.text1}>
            To the maximum extent permitted by applicable law,
            we exclude all representations, warranties and conditions relating to our Application and the use of this Application. Nothing in this disclaimer will:
          </Text>
          <Text style={styles.text1}>
            limit or exclude our or your liability for death or personal injury;
            limit or exclude our or your liability for fraud or fraudulent misrepresentation;
            limit any of our or your liabilities in any way that is not permitted under applicable law; or
            exclude any of our or your liabilities that may not be excluded under applicable law.
            The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and
            (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.
          </Text>
          <Text style={styles.text1}>
            As long as the Application and the information and services on the Application are provided free of charge, we will not be liable for any loss or damage of any nature.
          </Text>
        </View>
      </ScrollView>
    )
  }
}
function mapStateToProps(state) {
  return {
    Login: state.Login
  }
}
export default connect(mapStateToProps)(TermScreen);