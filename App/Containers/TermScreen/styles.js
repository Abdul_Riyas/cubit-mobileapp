import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    scrollbox:{
        flexGrow:1,
        backgroundColor:'#fff',
    },
    Container: {
        flex:1,
        backgroundColor:'#fff',
    },
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'OpenSans-SemiBold',
        fontSize:14

    },
    icon:{
        marginLeft:15,
        marginRight:15,
        fontSize:27,
        color:COLOR.White
    },
    box1:{
        flex:1,
        margin:20
    },
    aboutitem:{
        flexDirection:'row',
        justifyContent:'center',
        backgroundColor:'#F5F5F5',
        marginBottom:3
    },
    aboutitemtxt:{
        flex:1,
        fontFamily:'OpenSans-SemiBold',
        fontSize:14,
        margin:15
    },
    aboutitemicon:{
        fontSize:20,
        margin:15
    },
    text:{
        fontFamily:'OpenSans-SemiBold',
        fontSize:16,
        marginBottom:10
    },
    text1:{
        fontFamily:'OpenSans-Regular', 
        marginBottom:10
    }
   
})