import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flexGrow:1,
        backgroundColor:'#fff',
        flexDirection:'column'
    },
    headerTittle:{
        fontFamily:'OpenSans-SemiBlod',
        textAlign:'center',
        fontSize:15
    },
    box:{
        marginTop:-70,
        flex:1,
        margin:6
    },
})