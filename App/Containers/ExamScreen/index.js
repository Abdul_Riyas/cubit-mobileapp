import React, { Component } from 'react';
import { View, Text, StatusBar, ScrollView, FlatList } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderUser from '../../Components/HeaderUser';
import HeaderNotification from '../../Components/HeaderNotification';
import ExamItem from '../../Components/ExamItem';
import LottieView from 'lottie-react-native';
class ExamScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Cubit Academy',
            headerStyle: styles.headerStyle,
            headerTintColor: COLOR.White,
            headerTitleStyle: styles.headerTittle,
            headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary1]} style={{ flex: 1 }} />,
            headerLeft: () => <HeaderUser />,
            headerRight: () => <HeaderNotification />
        }
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.Container}>
                <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
                <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ height: 100 }} />
                <View style={{flex:1,margin:20}}>
                <LottieView
                    source={require('../../Image/soon.json')}
                    autoPlay
                  />
                </View>
                {/*<View style={styles.box}>
                    <FlatList
                        style={{ flex: 1 }}
                        initialNumToRender={10}
                        maxToRenderPerBatch={10}
                        data={[1, 2, 3, 4, 5, 6]}
                        renderItem={({ item, index }) =>
                            <View style={{ flex: 1, margin: 10 }}>
                                <ExamItem item={item} />
                            </View>}
                        keyExtractor={item => item}
                        numColumns={2}
                    />
                </View>*/}
            </ScrollView>
        )
    }
}
export default ExamScreen;