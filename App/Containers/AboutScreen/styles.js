import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    scrollbox:{
        flexGrow:1,
        backgroundColor:'#fff',
    },
    Container: {
        flex:1,
        backgroundColor:'#fff',
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'OpenSans-SemiBlod',
        fontSize:14,
        alignSelf: 'center'

    },
    icon:{
        marginLeft:15,
        marginRight:15,
        fontSize:27,
        color:COLOR.White
    },
    box1:{
        flex:1
    },
    aboutitem:{
        flexDirection:'row',
        justifyContent:'center',
        backgroundColor:'#F5F5F5',
        marginBottom:3
    },
    aboutitemtxt:{
        flex:1,
        fontFamily:'OpenSans-SemiBlod',
        fontSize:14,
        margin:15
    },
    aboutitemicon:{
        fontSize:20,
        margin:15
    }
   
})