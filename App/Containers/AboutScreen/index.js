import React, { Component } from 'react';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderUser from '../../Components/HeaderUser';
import HeaderNotification from '../../Components/HeaderNotification';
import { Icon } from 'native-base';
class AboutScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Cubit Academy',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />,
      headerLeft: () => <HeaderUser />,
      headerRight: () => <HeaderNotification />
    }
  }
  componentDidMount() {
    this.props.navigation.addListener('focus',
        payload => {
            StatusBar.setBarStyle('light-content');
            StatusBar.setBackgroundColor(COLOR.primary1);
        });
}
  render() {
    return (
      <ScrollView contentContainerStyle={styles.scrollbox}>
        <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
        <View style={styles.box1}>
          <TouchableOpacity style={styles.aboutitem} onPress={() => this.props.navigation.navigate('AppinfoScreen')}>
            <Text style={styles.aboutitemtxt}>About The app</Text>
            <Icon name="ios-arrow-forward" type="Ionicons" style={styles.aboutitemicon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.aboutitem} onPress={() => this.props.navigation.navigate('HelpScreen')}>
            <Text style={styles.aboutitemtxt}>Help desk</Text>
            <Icon name="ios-arrow-forward" type="Ionicons" style={styles.aboutitemicon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.aboutitem} onPress={() => this.props.navigation.navigate('PrivacyScreen')}>
            <Text style={styles.aboutitemtxt}>Privacy Policy</Text>
            <Icon name="ios-arrow-forward" type="Ionicons" style={styles.aboutitemicon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.aboutitem} onPress={() => this.props.navigation.navigate('TermScreen')}>
            <Text style={styles.aboutitemtxt}>Terms & Condition</Text>
            <Icon name="ios-arrow-forward" type="Ionicons" style={styles.aboutitemicon} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}
export default AboutScreen;