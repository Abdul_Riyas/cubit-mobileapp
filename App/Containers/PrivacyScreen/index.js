import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'native-base';
class PrivacyScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Privacy Policy',
            headerStyle: styles.headerStyle,
            headerTintColor: COLOR.White,
            headerTitleStyle: styles.headerTittle,
            headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('focus',
            payload => {
                StatusBar.setBarStyle('light-content');
                StatusBar.setBackgroundColor(COLOR.primary1);
            });
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scrollbox}>
                <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
                <View style={styles.box1}>
                    <Text style={styles.text}>Cubit application as a paid App. This service is provided by cubit academy at cost rs 2000/- for life Long and is intended for use as is


This page is used to inform visitors regarding my policies with the collection, use and disclosure of personal information if any one decided to use my service</Text>
                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
export default connect(mapStateToProps)(PrivacyScreen);