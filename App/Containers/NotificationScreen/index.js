import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './styles';
import COLOR from '../../Config/Color';
import { HeaderBackButton } from '@react-navigation/stack';
import { View, Text, Image, StatusBar,FlatList } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import API from '../../Config/API';
import PlaceHolder from './PlaceHolder';
import ErrorBox from '../../Components/ErrorBox';
import NodataBox from '../../Components/NodataBox';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
class NotificationScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Notifications',
            headerStyle: styles.headerStyle,
            headerTintColor: COLOR.White,
            headerTitleStyle: styles.headerTittle,
            headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />,
            headerLeft: (props) => <HeaderBackButton {...props}/>
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            errormsg: false,
            notifications: [],
        };
    }

    componentDidMount() {
        this.loaddata();
        this.props.navigation.addListener('focus',
            payload => {
                StatusBar.setBarStyle('light-content');
                StatusBar.setBackgroundColor(COLOR.primary1);
            });
    }

    loaddata = () => {
        fetch(API.BASE_URL + API.GET_NOTIFICATION, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.User.access_token
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.data) {
                    let data = responseJson.data;
                    this.setState({
                        notifications: data.notifications,
                        loading: false
                    })
                } else {
                    Toast.show('Something gone wrong.try again', ToastConfiq);
                    this.setState({ errormsg: true, loading: false })
                }
            })
            .catch((error) => {
                Toast.show('Something gone wrong.try again', ToastConfiq);
                this.setState({ errormsg: true, loading: false })
            })
    }
    render() {
        const { loading, errormsg, notifications } = this.state;
        if (errormsg) {
            return <ErrorBox refresh={this.loaddata} />
        } else {
            return (
                <View style={styles.Container}>
                    <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
                    {loading
                        ? <PlaceHolder />
                        : <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
                            {notifications.length === 0
                                ? <View style={styles.Container}><NodataBox /></View>
                                : <FlatList
                                    data={notifications}
                                    initialNumToRender={10}
                                    maxToRenderPerBatch={10}
                                    renderItem={({ item, index }) =>
                                        <View style={styles.listitem}>
                                            {item.image_url === null
                                                ? <Image
                                                    style={styles.notiimg}
                                                    source={require('../../Image/noti.png')}
                                                    resizeMode={'stretch'}
                                                />
                                                : <Image
                                                    style={styles.notiimg}
                                                    source={{ uri: API.IMAGE_PATH + item.image_url }}
                                                    resizeMode={'stretch'}
                                                />
                                            }
                                            <View style={styles.listbox}>
                                                <Text style={styles.listitemtxt1}>{item.title}</Text>
                                                <Text style={styles.listitemtxt2}>{item.message}</Text>
                                            </View>
                                        </View>}
                                    keyExtractor={index => index}
                                />
                            }
                        </View>
                    }
                </View>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        User: state.Login.User
    }
}
export default connect(mapStateToProps)(NotificationScreen);