import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    scrollbox: {
        flexGrow: 1,
        backgroundColor: '#fff',
    },
    Container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor: COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle: {
        color: COLOR.White,
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 14

    },
    icon: {
        marginLeft: 15,
        marginRight: 15,
        fontSize: 27,
        color: COLOR.White
    },
    box1: {
        flex: 1,
        backgroundColor: '#F5F5F5'
    },
    listitem: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 5,
        marginTop: 5,
        backgroundColor: COLOR.White,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    listbox: {
        margin: 20,
        flexDirection: 'column',
        flex: 1
    },
    listitemtxt1: {
        color: COLOR.black,
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 14,
        flex: 1
    },
    listitemtxt2: {
        color: "#484848",
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 12,
        marginTop: 4,
        flex: 1
    },
    notiimg: {
        width: 50,
        height: 50,
        marginLeft: 10
    }

})