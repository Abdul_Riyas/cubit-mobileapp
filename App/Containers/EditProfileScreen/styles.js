import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    scrollbox:{
        flexGrow:1,
        backgroundColor:'#fff',
    },
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'HelveticaNeue Medium',
        fontSize:14,

    },
    form:{
        margin:20,
        marginTop:0,
        flex:1
    },
    input:{
        flex:1,
        fontFamily:'HelveticaNeue Medium',
        padding:5,
        fontSize:14,
        textAlign:'left',
    },
    inputbox:{
        flexDirection:'row',
        borderBottomColor:COLOR.Gray1,
        borderBottomWidth:1,
        padding:0,
        alignItems:'center',
        marginTop:30
    },
    button2:{
        flex:1,
        marginTop:30
    },
    linearGradient1:{
        flexDirection:'row',
        alignItems:'center',
        borderRadius:50,
        padding:10,
        alignItems:'center',
        justifyContent:'center'
    },
    buttonText:{
        fontFamily:'HelveticaNeue Medium',
        fontSize:16,
        color:COLOR.White  
    },
    errorMessage:{
        fontFamily:'HelveticaNeue Medium',
        color:'red',
        marginTop:2,
        fontSize:12
    }
})