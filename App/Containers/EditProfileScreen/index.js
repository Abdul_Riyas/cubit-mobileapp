import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView, TextInput, ActivityIndicator } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Icon, Card } from 'native-base';
import API from '../../Config/API';
import moment from 'moment';
import { validateAll } from 'indicative/validator';
import { updateprofileService } from '../../Redux/Services/LoginServices';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
const rules = {
    name: 'required|string',
    email: 'required|email',
    phone: 'required|string'
}

const messages = {
    required: (field) => `${field} is required`,
    'name.string': 'Please enter name',
    'email.email': 'Please Enter your email',
    'phone.string': 'Please enter phone',
}
class EditProfileScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Edit User Informations',
            headerStyle: styles.headerStyle,
            headerTintColor: COLOR.White,
            headerTitleStyle: styles.headerTittle,
            headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            errormsg: null,
            name:(this.props.User.user && this.props.User.user.name) || null,
            email: (this.props.User.user && this.props.User.user.email) || null,
            phone: (this.props.User.user && this.props.User.user.mobile) || null,
            error: {}
        };
    }
    componentDidMount() {
        console.log(this.props.User)
        this.props.navigation.addListener('focus',
            payload => {
                StatusBar.setBarStyle('light-content');
                StatusBar.setBackgroundColor(COLOR.primary1);
            });
    }

    registration = (data) => {
        validateAll(data, rules, messages)
            .then((success) => {
                let obj = {
                    name: success.name,
                    email: success.email,
                    mobile: success.phone
                }
                this.update(obj);
                this.setState({ error: {} })
            })
            .catch((errors) => {
                const formattedErrors = {}
                errors.forEach(error => formattedErrors[error.field] = error.message);
                this.setState({ error: formattedErrors })
            })
    }

    update = (obj) => {
        this.setState({ loading: true })
        fetch(API.BASE_URL + API.UPDATE_USER + '/' + this.props.User.user.id, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.User.access_token
            },
            body: JSON.stringify(obj)
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === true) {
                    let respo = {
                        access_token:this.props.User.access_token,
                        user:responseJson.data.user
                    }
                    this.props.updateprofileService(respo);
                    this.setState({ loading: false })
                    this.props.navigation.goBack(null);
                }else{
                    Toast.show('Something went wrong.try again', ToastConfiq);
                }
            })
            .catch((error) => {
                Toast.show('Something went wrong.try again', ToastConfiq);
                this.setState({ loading: false });
            })
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scrollbox}>
                <StatusBar backgroundColor={COLOR.primary2} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <View style={styles.form}>
                        <View style={styles.inputbox}>
                            <TextInput
                                style={styles.input}
                                placeholder="Yor Name"
                                onChangeText={name => this.setState({ name })}
                                value={this.state.name} />
                        </View>
                        {this.state.error['name'] && <Text style={styles.errorMessage}>*{this.state.error['name']}</Text>}
                        <View style={styles.inputbox}>
                            <TextInput
                                style={styles.input}
                                placeholder="Email"
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email} />
                        </View>
                        {this.state.error['email'] && <Text style={styles.errorMessage}>*{this.state.error['email']}</Text>}
                        <View style={styles.inputbox}>
                            <TextInput
                                style={styles.input}
                                placeholder="Phone Number"
                                keyboardType="numeric"
                                onChangeText={phone => this.setState({ phone })}
                                value={this.state.phone} />
                        </View>
                        {this.state.error['phone'] && <Text style={styles.errorMessage}>*{this.state.error['phone']}</Text>}
                        {this.state.loading
                            ? <TouchableOpacity style={styles.button2}>
                                <LinearGradient colors={[COLOR.primary2, COLOR.primary1]} style={styles.linearGradient1}>
                                    <ActivityIndicator color="#fff" size="small" />
                                    <Text style={styles.buttonText}>Updating</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            : <TouchableOpacity onPress={() => this.registration(this.state)} style={styles.button2}>
                                <LinearGradient colors={[COLOR.primary2, COLOR.primary1]} style={styles.linearGradient1}>
                                    <Text style={styles.buttonText}>Update</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        }

                    </View>
                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        User: state.Login.User
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        updateprofileService: (data) => {
            dispatch(updateprofileService(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);