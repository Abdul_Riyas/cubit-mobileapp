import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flexGrow: 1,
        backgroundColor: '#fff',
        flexDirection: 'column'
    },
    headerTittle: {
        fontFamily: 'OpenSans-SemiBlod',
        textAlign: 'center',
        fontSize: 15
    },
    box: {
        marginTop: -100
    },
    imageslider: {
        flex: 1,
    },
    box1: {
        flex: 1,
        margin: 15,
        marginBottom: 0
    },
    heading: {
        fontFamily: 'OpenSans-Bold',
        color: '#000',
        fontSize: 16,
        margin: 10,
        marginTop: 0,
        flex: 1
    },
    box2: {
        flex: 1,
        margin: 15,
        marginRight: 0,
        marginLeft: 20
    },
    videocard: {
        width: 250,
        overflow: 'hidden',
        borderRadius: 5,
        marginRight:10,
    },
    videotxt1: {
        fontFamily: 'OpenSans-SemiBold',
        margin: 8,
        color: COLOR.black
    },
    videotxt2: {
        fontFamily: 'OpenSans-Regular',
        marginLeft: 8,
        marginBottom: 10,
        marginRight:8,
        fontSize:12,
        color: COLOR.Gray3
    },
    subheaderBox: {
        flexDirection: 'row'
    },
    viewallbtn: {
        color: COLOR.primary1,
        fontFamily: 'OpenSans-Bold',
        marginRight: 20
    },
    Banners: {
        width: 320,
        height: 180,
        margin: 10,
        marginLeft: 20
    }
})