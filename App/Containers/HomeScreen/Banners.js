import * as React from 'react';
import { ScrollView,ImageBackground,View} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import styles from './styles';
import API from '../../Config/API';
class Banners extends React.PureComponent {
  render() {
    return (
        <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        pagingEnabled={false}
        contentContainerStyle={{ flexGrow: 1 }}>
            {this.props.banners.map((item, index) =>
        <View style={styles.Banners} key={index}>
          <ImageBackground
            style={styles.imageslider}
            source={{uri:API.IMAGE_PATH+item.image_url}}
            resizeMode={'stretch'}
            borderRadius={13}
          />
        </View>
        )}
      </ScrollView>
    );
  }
}
export default withNavigation(Banners);
