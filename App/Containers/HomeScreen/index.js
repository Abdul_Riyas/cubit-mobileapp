import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar, ScrollView, TouchableOpacity } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderUser from '../../Components/HeaderUser';
import HeaderNotification from '../../Components/HeaderNotification';
import Banners from './Banners';
import Categories from './Categories';
import PopularVideos from './PopularVideos';
import API from '../../Config/API';
import PlaceHolder from './PlaceHolder';
import ErrorBox from '../../Components/ErrorBox';
import NodataBox from '../../Components/NodataBox';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
import moment from 'moment';
import ExperdBox from '../../Components/ExperdBox';
import { refreshuser } from '../../Redux/Services/LoginServices';
class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Cubit Academy',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary1]} style={{ flex: 1 }} />,
      headerLeft: () => <HeaderUser />,
      headerRight: () => <HeaderNotification />
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      errormsg: false,
      banners: [],
      popular_videos: [],
      courses: [],
      expired: false,
      data: [],
    };
  }

  componentDidMount() {
    this.loaddata();
    this.checkexpiredOn();
    this.props.refreshuser(this.props.User.access_token, this.props.User.user.id);
    this.props.navigation.addListener('focus',
      payload => {
        this.checkexpiredOn();
        //this.loaddata();
        this.props.refreshuser(this.props.User.access_token, this.props.User.user.id);
        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor(COLOR.primary1);
      })
  }

  checkexpiredOn = () => {
    let d = new Date();
    let currenttime = moment(d).format();
    let Trial_EndDate = moment(this.props.User.user.expire_on).format();
    var dateOne = new Date(currenttime);
    var dateTwo = new Date(Trial_EndDate);
    if (this.props.User.user.is_paid === 1 || dateTwo > dateOne) {
      this.setState({ expired: false })
    } else {
      this.setState({ expired: true })
    }
  }

  loaddata = () => {
    this.setState({ loading: true })
    fetch(API.BASE_URL + API.GET_HOME_DATA, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.User.access_token
      }
    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.data) {
          let data = responseJson.data;
          this.setState({
            banners: data.banners,
            popular_videos: data.popular_videos,
            courses: data.courses,
            loading: false,
            errormsg: false
          })
        } else {
          Toast.show(responseJson.message, ToastConfiq);
          this.setState({
            errormsg: true,
            loading: false
          })
        }
      })
      .catch((error) => {
        Toast.show('Something went wrong.try again', ToastConfiq);
        this.setState({
          errormsg: true,
          loading: false
        })
      })
  }
  render() {
    const { loading, courses, errormsg, expired, banners, popular_videos } = this.state;
    if (loading) {
      return (
        <PlaceHolder />
      )
    } else {
      return (
        <ScrollView contentContainerStyle={styles.Container}>
          <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
          <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ height: 100 }} />
          {expired === false
            ? <View style={{ flex: 1 }}>
              {errormsg !== false
                ? <ErrorBox refresh={this.loaddata} />
                : <View style={styles.box}>
                  {banners.length === 0
                    ? null
                    : <Banners banners={banners} />
                  }
                  {courses.length === 0
                    ? null
                    : <Categories courses={courses} />
                  }
                  {popular_videos.length === 0
                    ? null
                    : <PopularVideos popular_videos={popular_videos} />
                  }
                  {banners.length === 0 && courses.length === 0 && popular_videos.length === 0
                    ? <NodataBox />
                    : null
                  }
                </View>}
                <Text>{courses.length}</Text>
            </View>
            : <ExperdBox />

          }
        </ScrollView>
      )
    }
  }
}
function mapStateToProps(state) {
  return {
    User: state.Login.User
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    refreshuser: (token, userid) => {
      dispatch(refreshuser(token, userid));
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);