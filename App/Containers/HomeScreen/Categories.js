import React, { Component } from 'react';
import { View,Text,FlatList,TouchableOpacity} from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import SubCategoryItem from '../../Components/SubCategoryItem';
import { withNavigation } from '@react-navigation/compat';
class Categories extends React.PureComponent {
      constructor(props) {
        super(props);
        this.state = {
          loading: true,
          errormsg:null
        };
      }

    render() {
        return (
            <View style={styles.box1}>
              <View style={styles.subheaderBox}>
              <Text style={styles.heading}>Categories</Text>
              <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Categories')}>
                <Text style={styles.viewallbtn}>View All</Text>
              </TouchableOpacity>
              </View>
            
            <FlatList
            style={{ flex: 1 }}
            initialNumToRender={10}
            maxToRenderPerBatch={10}
            data={this.props.courses.splice(0,4)}
            renderItem={({ item, index }) =>
              <View style={{flex:1,margin:10}}>
                  <SubCategoryItem item={item}/>
              </View>}
            keyExtractor={item => item.id}
            numColumns={2}
          />
          </View>
        )
    }
}
export default withNavigation(Categories);