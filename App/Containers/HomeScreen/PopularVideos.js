import React, { Component } from 'react';
import { View, Text, StatusBar, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import { Icon, Card } from 'native-base';
import { WebView } from 'react-native-webview';
import { withNavigation } from '@react-navigation/compat';
class PopularVideos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            errormsg: null
        };
    }
    render() {
        return (
            <View style={styles.box2}>
                <View style={styles.subheaderBox}>
                    <Text style={styles.heading}>Popular Videos</Text>
                    <TouchableOpacity style={{ marginRight: 10 }}
                        onPress={() => this.props.navigation.navigate('PopularVideoScreen', {data:this.props.popular_videos})}>
                        <Text style={styles.viewallbtn}>View All</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView
                    contentContainerStyle={{ flexGrow: 1 }}
                    horizontal
                    showsHorizontalScrollIndicator={false}>
                    {this.props.popular_videos.map((item) => {
                        return (
                            <TouchableOpacity key={item.id}
                                onPress={() => this.props.navigation.navigate('VideoPlayerScreen', { data: item })}>
                                <Card style={styles.videocard}>
                                    <WebView
                                        mediaPlaybackRequiresUserAction={true}
                                        allowsFullscreenVideo={true}
                                        javaScriptEnabled={true}
                                        allowsInlineMediaPlayback={true}
                                        startInLoadingState={true}
                                        allowsInlineMediaPlayback={true}
                                        style={{ width: 250, height: 145, backgroundColor: '#fff' }}
                                        source={{
                                            uri: 'https://player.vimeo.com/video/' + item.video_url + '?transparent=0&title=0&byline=0&portrait=0&sidedock=0&controls=false&autoplay=false' //'this.state.videoUrl'
                                        }}
                                        onError={() => console.log('on error')}
                                        bounces={false}
                                    />
                                    <View>
                                        <Text style={styles.videotxt1} numberOfLines={2}>{item.title}</Text>
                                        <Text style={styles.videotxt2} numberOfLines={2}>{item.description}</Text>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}
export default withNavigation(PopularVideos);