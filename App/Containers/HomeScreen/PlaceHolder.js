import * as React from 'react';
import {View} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import ContentLoader from "react-native-easy-content-loader";
class PlaceHolder extends React.PureComponent {
  render() {
    return (
        <View style={{flex:1,marginTop: -80}}>
        <ContentLoader 
        active
        pHeight={[180, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={1} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
        <View style={{flexDirection:'row',marginBottom:10,marginTop:10}}>
        <View style={{ flex: 1 }}>
        <ContentLoader 
        active
        pHeight={[150, 10]} 
        pWidth={[0, 150, 100]}
        title={false} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
        </View>
        <View style={{ flex: 1 }}>
        <ContentLoader 
        active
        pHeight={[150, 10]} 
        pWidth={[0, 150, 100]}
        title={false} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 0.5)'}
        style={{borderRadius:20}} 
        />
        </View>
        </View>
        <ContentLoader 
        active
        pHeight={[150, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
      </View>
    );
  }
}
export default withNavigation(PlaceHolder);
