import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar, ScrollView, FlatList } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderUser from '../../Components/HeaderUser';
import HeaderNotification from '../../Components/HeaderNotification';
import SubCategoryItem from '../../Components/SubCategoryItem';
import API from '../../Config/API';
import PlaceHolder from './PlaceHolder';
import ErrorBox from '../../Components/ErrorBox';
import NodataBox from '../../Components/NodataBox';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
import moment from 'moment';
import ExperdBox from '../../Components/ExperdBox';
class CategorieScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Cubit Academy',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary1]} style={{ flex: 1 }} />,
      headerLeft: () => <HeaderUser />,
      headerRight: () => <HeaderNotification />
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      errormsg: false,
      courses: [],
      expired:false,
    };
  }

  componentDidMount() {
    this.loaddata();
    this.checkexpiredOn();
    this.props.navigation.addListener('focus',
    payload => {
      this.checkexpiredOn();
      this.forceUpdate()
      StatusBar.setBarStyle('light-content');
      StatusBar.setBackgroundColor(COLOR.primary1);
    });
  }


  checkexpiredOn = () =>{
    let d = new Date();
    let currenttime = moment(d).format();
    let Trial_EndDate = moment(this.props.User.user.expire_on).format();
    var dateOne = new Date(currenttime);
    var dateTwo = new Date(Trial_EndDate);
    if (this.props.User.user.is_paid === 1 || dateTwo > dateOne) {
      this.setState({expired:false})
    } else {
      this.setState({expired:true})
    }
  }

  loaddata = () => {
    fetch(API.BASE_URL + API.GET_CATEGORIES, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.User.access_token
      }
    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.data) {
          let data = responseJson.data;
          this.setState({
            courses: data.courses,
            loading: false
          })
        } else {
          Toast.show('Something gone wrong.try again', ToastConfiq);
          this.setState({ errormsg: true, loading: false })
        }
      })
      .catch((error) => {
        Toast.show('Something gone wrong.try again', ToastConfiq);
        this.setState({ errormsg: true, loading: false })
      })
  }
  render() {
    const { loading, courses,errormsg,expired } = this.state;
  if (errormsg) {
      return <ErrorBox refresh={this.loaddata} />
  }else{
    return (
      <ScrollView contentContainerStyle={styles.Container}>
        <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
        <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ height: 100 }} />
        {expired === false
        ?<View style={{ flex: 1 }}>
        {loading
          ? <PlaceHolder />
          : <View style={styles.box}>
            {courses.length === 0
              ? <NodataBox />
              : <FlatList
                style={{ flex: 1 }}
                initialNumToRender={10}
                maxToRenderPerBatch={10}
                data={courses}
                renderItem={({ item, index }) =>
                  <View style={{ flex: 1, margin: 10 }}>
                    <SubCategoryItem item={item} />
                  </View>}
                keyExtractor={item => item.id}
                numColumns={2}
              />}
          </View>
        }</View>
        :<ExperdBox/>
        }
      </ScrollView>
    )
  }
}}
function mapStateToProps(state) {
  return {
    User: state.Login.User
  }
}
export default connect(mapStateToProps)(CategorieScreen);