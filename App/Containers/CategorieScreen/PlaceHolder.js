import * as React from 'react';
import {View} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import ContentLoader from "react-native-easy-content-loader";
class PlaceHolder extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1,marginTop:-80}}>
      {[1, 2, 3, 4, 5, 6, 7, 8].map(function (item, i) {
        return <View style={{ flexDirection: 'row', margin: 8 }} key={i}>
          <View style={{ flex: 1 }}>
            <ContentLoader active pHeight={[70, 10]} title={false} pWidth={[0, 150, 100]} pRows={3} primaryColor={'rgba(220, 220, 220, 0.5)'} />
          </View>
          <View style={{ flex: 1 }}>
            <ContentLoader active pHeight={[70, 10]} title={false} pWidth={[0, 150, 100]} pRows={3} primaryColor={'rgba(220, 220, 220, 0.5)'} />
          </View>
        </View>
      })}
    </View>
    );
  }
}
export default withNavigation(PlaceHolder);
