import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flex:1,
        backgroundColor:'#fff',
        flexDirection:'column',
        alignItems:'center'
    },
    box:{
        flex:1,
        justifyContent:'center'
    },
    logo:{
        width:245,
        height:102
    },
    brand:{
        color:COLOR.Gray4,
        fontFamily:'OpenSans-SemiBold',
        alignItems:'flex-end',
        marginBottom:10,
        fontSize:13
    }
})