import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StatusBar, Image, Text } from 'react-native';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import styles from './styles';
class SplashScreen extends Component {
    async componentDidMount() {
        const authorizationStatus = await messaging().requestPermission();
        if (authorizationStatus) {
            const token = await messaging().getToken();
            //console.log('messagingrequestPermission - ture', token);
        }
        setTimeout(() => {
            this._bootstrapAsync();
        }, 1000);

        if (this.props.Login.auth === false) {
            auth().onAuthStateChanged((user) => {
                if (user) {
                    auth().signOut();
                }
            })
        }
    }
    _bootstrapAsync = () => {
        this.props.navigation.reset(
            this.props.Login.auth ?
                { routes: [{ name: 'User' }] } : (
                    this.props.Login.intro ?
                        { routes: [{ name: 'IntorScreen' }] } :
                        { routes: [{ name: 'LoginScreen' }] }
                ))
    }
    render() {
        return (
            <View style={styles.Container}>
                <StatusBar translucent backgroundColor="transparent" />
                <View style={styles.box}>
                    <Image style={styles.logo} resizeMode='center'
                        source={require('../../Image/logo.png')} />
                </View>
                <Text style={styles.brand}>Powder By Optimist Tech Hub</Text>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
export default connect(mapStateToProps)(SplashScreen);