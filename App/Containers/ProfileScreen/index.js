import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View,Text,ScrollView,Image,TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import LogoutButton from './LogoutButton';
import styles from './styles';
import COLOR from '../../Config/Color';
class ProfileScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          title: 'Profile',
          headerStyle: styles.headerStyle,
          headerTintColor: COLOR.White,
          headerTitleStyle: styles.headerTittle,
          headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary1]} style={{ flex: 1 }} />,
          headerRight: () => <LogoutButton />
        }
      }
      componentDidMount(){
          //alert(JSON.stringify(this.props.User))
      }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.ScrollContainer}>
                 <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={styles.linearGradient}>
                 <Image
              style={styles.profile}
              source={require('../../Image/avatar.png')}
              resizeMode={'stretch'}
            />
            <Text style={styles.name}>{(this.props.User.user && this.props.User.user.name) || null}</Text>
            <Text style={styles.mobile}>{(this.props.User.user && this.props.User.user.mobile) || null}</Text>
                 </LinearGradient>
                 <View style={styles.itembox2}>
              <View style={styles.iteminnerbox}>
                <Text style={styles.itemtxt1}>Email</Text>
                <Text style={styles.itemtxt2}>{(this.props.User.user && this.props.User.user.email) || null}</Text>
              </View>
            </View>
            <TouchableOpacity style={styles.itembox}
            onPress={()=>this.props.navigation.navigate('EditProfileScreen')}>
            <View style={styles.iteminnerbox}>
              <Text style={styles.editxt}>Edit Profile</Text>
            </View>
          </TouchableOpacity>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
      User: state.Login.User
    }
  }
  export default connect(mapStateToProps)(ProfileScreen);