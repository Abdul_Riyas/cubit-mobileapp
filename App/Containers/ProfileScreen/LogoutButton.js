import React, { Component } from 'react';
import { connect } from 'react-redux';
import {TouchableOpacity} from 'react-native';
import { Icon} from 'native-base';
import { withNavigation } from '@react-navigation/compat';
import { logoutService } from '../../Redux/Services/LoginServices';
class LogoutButton extends Component {
    render() {
        return (
            <TouchableOpacity
            onPress={() => { this.props.logoutService(this.props.User.access_token) }}>
             <Icon name="logout" type="MaterialCommunityIcons" style={{color:'#fff',marginRight:15,fontSize:25}} />
          </TouchableOpacity>
        )
    }
}
function mapStateToProps(state) {
    return {
        User: state.Login.User
    }
  }
  const mapDispatchToProps = (dispatch) => {
    return {
      logoutService: (token) => {
        dispatch(logoutService(token));
      },
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(LogoutButton));