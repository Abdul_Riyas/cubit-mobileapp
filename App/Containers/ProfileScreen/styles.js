import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.Gray1,
        borderBottomWidth:0,
        backgroundColor: COLOR.black
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'OpenSans-SemiBold',
        fontSize:14
    },
    ScrollContainer: {
        flexGrow:1,
        backgroundColor:COLOR.Gray1
    },
    linearGradientBtn:{
        borderRadius:50,
        padding:7
    },
    profile:{
        width:80,
        height:80,
        borderRadius:50,
        alignSelf:'center',
        margin:10
    },
    name:{
        color:'#fff',
        fontSize:16,
        textAlign:'center',
        fontFamily:'OpenSans-SemiBold',
        margin:10
    },
    mobile:{
        color:'#fff',
        fontSize:14,
        textAlign:'center',
        fontFamily:'OpenSans-Regular',
        marginBottom:30
    },
    itembox:{
        backgroundColor:COLOR.White,
        marginBottom:2
    },
    itembox2:{
        backgroundColor:COLOR.White,
        marginBottom:5,
        marginTop:5
    },
    iteminnerbox:{
        margin:20
    },
    iteminnerbox2:{
        flexDirection:'row',
        justifyContent:'center'
    },
    itemtxt1:{
        fontFamily:'OpenSans-SemiBold',
        fontSize:14 ,
        color:COLOR.black
    },
    itemtxt2:{
        fontFamily:'OpenSans-Regular',
        fontSize:12,
        color:'#767676',
        marginTop:6
    },
    editxt:{
        fontFamily:'OpenSans-SemiBold',
        fontSize:14,
        color:COLOR.primary1
    }
})