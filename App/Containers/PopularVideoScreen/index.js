import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text,StatusBar,FlatList} from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
import HeaderNotification from '../../Components/HeaderNotification';
import NodataBox from '../../Components/NodataBox';
import VideoItem from '../../Components/VideoItem';
class PopularVideoScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Popular Videos',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />,
      headerRight: () => <HeaderNotification />
    }
  }

  constructor(props) {
    super(props);
    this.state = {
        loading: true,
        error: null,
        datasource: (this.props.route.params && this.props.route.params.data) || []
    };
}

  componentDidMount() {
    this.props.navigation.addListener('focus',
      payload => {
        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor(COLOR.primary1);
      });
  }

  show =(data)=>{
    console.log(data);
    return 'data';
  }

  render() {
    return (
      <View style={styles.Container}>
        <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
        {this.props.route.params.data.length === 0
          ? <NodataBox />
          :<View style={styles.box1}>
            <FlatList
                initialNumToRender={10}
                maxToRenderPerBatch={10}
                updateCellsBatchingPeriod={50}
                data={this.state.datasource}
                renderItem={(items,index) =>
                  <VideoItem items={items.item}/>
                }
                keyExtractor={(items) => items.item}
              />
            </View>
            }
        </View>
    )
  }
}
function mapStateToProps(state) {
  return {
    Login: state.Login
  }
}
export default connect(mapStateToProps)(PopularVideoScreen);