import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flex:1,
        backgroundColor:'#fff',
    },
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'OpenSans-SemiBold',
        fontSize:14
    },
    box1:{
        flex:1
    },
})