import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    scrollbox:{
        flexGrow:1,
        backgroundColor:'#fff',
    },
    Container: {
        flex:1,
        backgroundColor:'#fff',
    },
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle:{
        color:COLOR.White,
        fontFamily:'OpenSans-SemiBold',
        fontSize:14

    },
    icon:{
        marginLeft:15,
        marginRight:15,
        fontSize:27,
        color:COLOR.White
    },
    box1:{
        flex:1,
        margin:20,
        alignItems:'stretch'
    },
    text:{
        fontFamily:'OpenSans-SemiBold',
        fontSize:16
    }
   
})