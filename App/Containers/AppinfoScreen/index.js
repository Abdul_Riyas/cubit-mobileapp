import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar, ScrollView } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
class AppinfoScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'About the app',
            headerStyle: styles.headerStyle,
            headerTintColor: COLOR.White,
            headerTitleStyle: styles.headerTittle,
            headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('focus',
            payload => {
                StatusBar.setBarStyle('light-content');
                StatusBar.setBackgroundColor(COLOR.primary1);
            });
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scrollbox}>
                <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
                <View style={styles.box1}>
                    <Text style={styles.text}>Co-operative (സഹകരണ) മേഖലയിൽ നടത്തപ്പെടുന്ന മത്സര പരീക്ഷകളെ (CSEB, PSC, Society exams, Board exams) വിജയകരമായി നേരിടുന്നതിനാവശ്യമായ മുഴുവൻ Syllabus ഉം Basic മുതൽ അവസാനം വരേ വളരേ ലളിതമായും ,വ്യക്കതമായും അവതരിപ്പിക്കുകയാണ് സഹകരണ പരീക്ഷ മേഖലയിൽ 10 വർഷത്തിലതികമായി പരിചയ സമ്പത്തുള്ള 

കൂടാതെ നാളിത് വരേ സഹകരണ മേഖലയിൽ നടത്തിയിട്ടുള്ള PSC, CSEB , Society Previous Question Paper ഉം ഉദ്യാഗാർത്ഥികൾക്ക് വേണ്ടി ഈ application ൽ ഉൾപ്പെടുത്തിയിരിക്കുന്നു</Text>
                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
export default connect(mapStateToProps)(AppinfoScreen);