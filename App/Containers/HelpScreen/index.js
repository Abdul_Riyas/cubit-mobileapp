import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView, TextInput, ActivityIndicator, Linking } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'native-base';
import { validateAll } from 'indicative/validator';
import API from '../../Config/API';
const rules = {
  subject: 'required|string',
  Message: 'required|string',
}

const messages = {
  required: (field) => `${field} is required`,
  'subject.string': 'Please enter name',
  'Message.string': 'Please Enter your email',
}
class HelpScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Help Desk',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      errormsg: false,
      success: false,
      subject: null,
      Message: null,
      error: {}
    };
  }

  componentDidMount() {
    this.props.navigation.addListener('focus',
      payload => {
        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor(COLOR.primary1);
      });
  }

  sendData = (data) => {
    validateAll(data, rules, messages)
      .then((success) => {
        let obj = {
          title: success.subject,
          messege: success.Message
        }
        this.send(obj);
        this.setState({ error: {} })
      })
      .catch((errors) => {
        const formattedErrors = {}
        errors.forEach(error => formattedErrors[error.field] = error.message);
        this.setState({ error: formattedErrors })
      })
  }

  send = (obj) => {
    this.setState({ loading: true })
    fetch(API.BASE_URL + API.POST_FEEDBACK, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.User.access_token
      },
      body: JSON.stringify(obj)
    }).then((response) => response.json())
      .then((responseJson) => {
        //alert(JSON.stringify(responseJson))
        if (responseJson.data) {
          this.setState({
            loading: false,
            success: true,
            subject: null,
            Message: null
          })
        } else {
         
          this.setState({ loading: false, errormsg: true })
        }
      })
      .catch((error) => {
        this.setState({ loading: false, errormsg: true })
        alert(JSON.stringify(error))
      })
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.scrollbox}>
        <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
        <View style={styles.box1}>
          <TouchableOpacity style={styles.call2} onPress={() => { Linking.openURL(`tel:+919447721923`) }}>
            <Icon name="phone" type="AntDesign" style={styles.icons} />
            <Text style={styles.phone}>+91 94477 21923</Text>
          </TouchableOpacity>
          <View>

            <View style={styles.inputbox}>
              <TextInput
                placeholder="Subject"
                style={styles.input}
                onChangeText={(subject) => this.setState({ subject })}
                value={this.state.subject}
              />
            </View>
            {this.state.error['subject'] && <Text style={styles.errorMessage}>*{this.state.error['subject']}</Text>}
            <View style={styles.inputbox}>
              <TextInput
                placeholder="Message"
                style={styles.input1}
                numberOfLines={10}
                multiline={true}
                onChangeText={(Message) => this.setState({ Message })}
                value={this.state.Message}
              />
            </View>
            {this.state.error['Message'] && <Text style={styles.errorMessage}>*{this.state.error['Message']}</Text>}
            {this.state.success
              ? <View style={styles.successbox}>
                <Text style={styles.successtxt1}>Thank You.</Text>
                <Text style={styles.successtxt2}>Your Message sent Successfully</Text>
              </View>
              : null
            }
            {this.state.errormsg
              ? <View style={styles.errorBox}>
                <Text style={styles.successtxt1}>faild.</Text>
                <Text style={styles.successtxt2}>Try again</Text>
              </View>
              : null
            }

            <TouchableOpacity style={styles.button2}
              onPress={() => { this.state.loading ? null : this.sendData(this.state) }}>
              <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={styles.linearGradient1}>
                {this.state.loading
                  ? <ActivityIndicator color="#fff" size="small" />
                  : <Text style={styles.buttonText}>Send</Text>}
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }
}
function mapStateToProps(state) {
  return {
    User: state.Login.User
  }
}
export default connect(mapStateToProps)(HelpScreen);