import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { WebView } from 'react-native-webview';
import styles from './styles';
import { Icon } from 'native-base';
class ListItem extends React.PureComponent {
  getVimeoPageURL(id) {
    return 'https://player.vimeo.com/video/'+id+'?transparent=0&title=0&byline=0&portrait=0&sidedock=0&controls=false&autoplay=false';
  }
  render() {
    return (
      <TouchableOpacity
      onPress={() =>{
        this.props.navigation.navigate('VideoPlayerScreen',{data:this.props.items})
    }}>
        <View style={{ backgroundColor: 'black'}}>
          <WebView
            mediaPlaybackRequiresUserAction={true} 
            allowsFullscreenVideo={true}
            javaScriptEnabled={true}
            allowsInlineMediaPlayback={true}
            startInLoadingState = {true}
            allowsInlineMediaPlayback = {false}
            renderLoading={() =><View style={{flex:1,alignItems:'center'}}>
              <Text>Loading....</Text>
              </View>}
            ref="webviewBridge"
            style={{
              minHeight:300
            }}
            source={{ uri: this.getVimeoPageURL(this.props.items.video_url) }}
            onError={error => console.error(error)}
          />
        </View>
        <View style={{ padding:15, borderTopWidth: 1, borderColor: 'black', flexDirection: 'row' }}>
            <Text style={{ fontFamily: 'OpenSans-SemiBold',fontSize:15,flex:1}}>{this.props.items.video_title}</Text>
            <View style={styles.listactionbox}>
              {this.props.items.document_one_file_name !==null
              ?<TouchableOpacity style={styles.downloadbtn}
              onPress={()=>this.props.download(this.props.items.document_one_file_name,this.props.items.document_one_file_url)}>
                <Text style={styles.downloadbtntxt}>LHS</Text>
                <Icon name="file-download" type="MaterialIcons" style={styles.downloadbicon}/>
                </TouchableOpacity>
              :null
              }
              {this.props.items.document_two_file_name !==null
              ?<TouchableOpacity style={styles.downloadbtn}
              onPress={()=>this.props.download(this.props.items.document_two_file_name,this.props.items.document_two_file_url)}>
              <Text style={styles.downloadbtntxt}>RHS</Text>
              <Icon name="file-download" type="MaterialIcons" style={styles.downloadbicon}/>
              </TouchableOpacity>
              :null
              }
              
                
            </View>
          <View style={{flexDirection:'row'}}>

          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
export default withNavigation(ListItem);
