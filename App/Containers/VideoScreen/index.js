import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar,FlatList, ActivityIndicator,PermissionsAndroid } from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderNotification from '../../Components/HeaderNotification';
import DiscussButton from './DiscussButton';
import API from '../../Config/API';
import PlaceHolder from './PlaceHolder';
import ErrorBox from '../../Components/ErrorBox';
import NodataBox from '../../Components/NodataBox';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
import RNFetchBlob from 'react-native-fetch-blob';
import ListItem from './ListItem';
class VideoScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Cubit Academy',
      headerStyle: styles.headerStyle,
      headerTintColor: COLOR.White,
      headerTitleStyle: styles.headerTittle,
      headerBackground: () => <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{ flex: 1 }} />,
      headerRight: () => <HeaderNotification />
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      errormsg: null,
      datasource: [],
      videos: [],
      expired: false,
      page: 1,
      nodata: false
    };
    this.onEndReached = this.onEndReached.bind(this);
    this.page = 1;
  }


  componentDidMount() {
    this.loaddata();
    this.setState({ page: 1 });
    this.props.navigation.addListener('focus',
      payload => {
        this.setState({ page: 1 });
        StatusBar.setBarStyle('light-content');
        StatusBar.setBackgroundColor(COLOR.primary1);
      });
  }

  loaddata = () => {
    fetch(API.BASE_URL + API.GET_COURSE_VIDEO + '/' + this.props.route.params.item.id + '?page=' + this.page, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.User.access_token
      }
    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.data) {
          if (responseJson.data.length !== 0) {
            let data = responseJson.data[0].urls;
            let listData = this.state.videos;
            let datas = listData.concat(data)
            this.setState({
              videos: datas,
              datasource: responseJson.data,
              loading: false,
              nodata: false,
              errormsg:false
            })
          } else {
            this.setState({ nodata: true, loading: false })
          }
        } else {
          Toast.show('Something went wrong.try again', ToastConfiq);
          this.setState({ nodata: true, loading: false, videos: [] })
        }
      })
      .catch((error) => {
        Toast.show('Something went wrong.try again', ToastConfiq);
        alert(JSON.stringify(error))
        this.setState({ nodata: true,errormsg:true,loading: false })
      })
  }

  downloadNow = (filename, fileurl) => {
    console.log(filename);
    const downloads = RNFetchBlob.fs.dirs.DownloadDir;
    RNFetchBlob
      .config({
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          mediaScannable: true,
          path: downloads + '/' + filename
        }
      })
      .fetch('GET', API.FILE_PATH + fileurl, {
      }).then((res) => {
        //console.log(res);
        console.log('The file saved to ', res.path());
        Toast.show('Download completed', ToastConfiq);
      })
      .catch((errorMessage) => {
        console.log(errorMessage);
        Toast.show('Something went wrong.try again', ToastConfiq);
      })
  }

  download = async (filename, fileurl) => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.downloadNow(filename, fileurl);
      } else {
        Alert.alert('Permission Denied!', 'You need to give storage permission to download the file');
      }
    } catch (err) {
      Toast.show('Something went wrong.try again', ToastConfiq);
      console.log(err);
    }
  }

  onEndReached = () => {
    if (!this.onEndReachedCalledDuringMomentum) {
      if (!this.state.nodata) {
        this.page = this.page + 1;
        this.loaddata();
      }
      this.onEndReachedCalledDuringMomentum = true;
    }
  }

  renderFooter = () => {
    return (
      <View>
        {!this.onEndReachedCalledDuringMomentum
          ? <ActivityIndicator size="small" color="#000" style={styles.loading} />
          : null}
      </View>
    )
  }

  render() {
    if (this.state.errormsg) {
      return <ErrorBox refresh={this.loaddata} />
  }else{
    return (
      <View style={styles.Container}>
        {this.state.loading
          ? <PlaceHolder />
          : <View style={{ flex: 1 }}>
            {this.state.videos.length === 0
              ? <NodataBox />
              : <FlatList
                style={{ flex: 1 }}
                ListHeaderComponent={<View style={styles.headerBox}>
                  {this.state.datasource.map((item) => {
                    return (
                      <View style={{ flex: 1 }} key={item.id}>
                        <Text style={styles.headertxt}>{item.title}</Text>
                        <Text style={styles.headersubtxt}>{item.description}</Text>
                      </View>
                    )
                  })}
                  <DiscussButton id={this.props.route.params.item.id}/>
                </View>}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.videos}
                onEndReachedThreshold={0.5}
                onEndThreshold={0}
                initialNumToRender={10}
                onEndReached={this.onEndReached}
                renderItem={({ item, index }) => (
                  <View>
                    <ListItem
                      items={item}
                      download={this.download}
                    />
                  </View>
                )}
                ListFooterComponent={this.renderFooter.bind(this)}
                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
              />}</View>}
      </View>
    )
  }
}
}
function mapStateToProps(state) {
  return {
    User: state.Login.User
  }
}
export default connect(mapStateToProps)(VideoScreen);