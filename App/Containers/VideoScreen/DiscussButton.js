import React, { Component } from 'react';
import {TouchableOpacity,Text} from 'react-native';
import { Icon} from 'native-base';
import { withNavigation } from '@react-navigation/compat';
import COLOR from '../../Config/Color';
class DiscussButton extends Component {
    render() {
        return (
            <TouchableOpacity
            onPress={() =>{
                this.props.navigation.navigate('ChatScreen',{data:this.props.id})
            }}>
             <Icon name="chat" type="Entypo" style={{color:COLOR.primary1,margin:10,marginTop:0}} />
          </TouchableOpacity>
        )
    }
}
export default withNavigation(DiscussButton);