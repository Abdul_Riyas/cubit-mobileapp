import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flex:1,
        backgroundColor:'#fff',
    },
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth: .9,
        backgroundColor: COLOR.White,
    },
    headerTittle: {
        fontFamily: 'OpenSans-SemiBlod',
        textAlign: 'center',
        fontSize: 15
    },
    box1:{
        flex:1
    },
    headerBox:{
        flexDirection:'row',
        margin:15
    },
    headertxt:{
        flex:1,
        fontFamily: 'OpenSans-SemiBlod',
        fontSize:17,
        color:COLOR.primary1
    },
    headersubtxt:{
        marginTop:7,
        fontSize:13,
        fontFamily: 'OpenSans-Regular',
        color:COLOR.Gray3
    },
    listactionbox:{
        flexDirection:'row'
    },
    downloadbtn:{
        backgroundColor:COLOR.Gray3,
        borderRadius:50,
        padding:5,
        flexDirection:'row',
        alignItems:'center',
        marginLeft:10,
        height:30
    },
    downloadbtntxt:{
        fontFamily: 'OpenSans-SemiBlod',
        fontSize:12,
        color:'#fff',
        marginLeft:15,
        margin:5
    },
    downloadbicon:{
        marginRight:15,
        fontSize:20,
        color:'#fff'
    }
})