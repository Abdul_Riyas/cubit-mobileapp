import * as React from 'react';
import {View} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import ContentLoader from "react-native-easy-content-loader";
class PlaceHolder extends React.PureComponent {
  render() {
    return (
        <View style={{flex:1,marginTop:10}}>
        <ContentLoader 
        active
        pHeight={[180, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
        <ContentLoader 
        active
        pHeight={[180, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
         <ContentLoader 
        active
        pHeight={[180, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
        <ContentLoader 
        active
        pHeight={[150, 10]} 
        title={false} 
        pWidth={[0, 150, 100]} 
        pRows={3} 
        primaryColor={'rgba(220, 220, 220, 220)'}
        style={{borderRadius:20}} 
        />
      </View>
    );
  }
}
export default withNavigation(PlaceHolder);
