import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StatusBar, Text, ScrollView} from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import moment from 'moment';
import { WebView } from 'react-native-webview';
class VideoPlayerScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: '',
            headerTintColor: '#fff',
            headerTitleStyle: styles.headerTittle,
            headerStyle: styles.headerStyle,
        }
    }
    getVimeoPageURL() {
        return 'https://player.vimeo.com/video/'+this.props.route.params.data.video_url+'?transparent=0&title=0&byline=0&portrait=0&controls=true';
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.Container}>
                <StatusBar backgroundColor={COLOR.black} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Text style={styles.heading}>{  (this.props.route.params.data && this.props.route.params.data.title) || null}</Text>
                    <WebView
                        mediaPlaybackRequiresUserAction={true}
                        allowsFullscreenVideo={true}
                        javaScriptEnabled={true}
                        allowsInlineMediaPlayback={true}
                        startInLoadingState={true}
                        allowsInlineMediaPlayback={true}
                        ref="webviewBridge"
                        style={{
                            borderWidth: 1,
                            borderColor: 'red',
                            borderRadius: 10,
                            backgroundColor: '#000'
                        }}
                        source={{ uri: this.getVimeoPageURL() }}
                        onError={error => console.error(error)}
                    />
                </View>
                <Text style={styles.subheading}>{  (this.props.route.params.data && this.props.route.params.data.description) || null}</Text>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
export default connect(mapStateToProps)(VideoPlayerScreen);