import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    headerStyle:{
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.Gray1,
        borderBottomWidth:0,
        backgroundColor: COLOR.black
    },
    headerTittle:{
        fontFamily:'OpenSans-SemiBold',
    },
    Container: {
        flexGrow:1,
        backgroundColor:COLOR.black
    },
    box:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        height:win.height
    },
    alertimg:{
        width:90,
        height:90
    },
    trialtxt:{
        fontFamily:'OpenSans-SemiBold',
        color:COLOR.Gray4,
        fontSize:17,
        marginTop:10  
    },
    heading:{
        fontFamily:'OpenSans-SemiBold',
        color:'#fff',
        marginLeft:15,
        fontSize:20
    },
    subheading:{
        fontFamily:'OpenSans-SemiBold',
        color:'#fff',
        marginLeft:15,
        fontSize:15,
        marginBottom:10,
        marginTop:10
    },
    date:{
        fontFamily:'OpenSans-SemiBold',
        color:'#fff',
        marginLeft:15,
        fontSize:10,
        marginBottom:10
    }
})