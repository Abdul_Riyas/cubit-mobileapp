import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar, ScrollView,Image,TouchableOpacity } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import { introService } from '../../Redux/Services/LoginServices';
class IntorScreen extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.Container}>
                <StatusBar backgroundColor={COLOR.primary3} barStyle="dark-content" />
                <View style={styles.box1}>
                    <View style={styles.box2}>
                    <Image style={styles.logo} resizeMode='center'
                    source={require('../../Image/splash.png')} />
                    </View>
                    <View style={styles.box3}>
                        <Text style={styles.Text1}>Cubit Academy</Text>
                        <Text style={styles.Text2}>E learning App</Text>
                        <Text style={styles.Text3}>Curabitur cursus sit amet elit in mattis. Pellentesque quis sodales nisl. Maecenas fringilla, odio molestie eleifend scelerisque</Text>
                    <TouchableOpacity style={styles.nextbtn}
                    onPress={()=> this.props.introService()}>
                        <Text style={styles.nextbtntxt}>Next</Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        introService: () => {
            dispatch(introService());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(IntorScreen);