import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, StatusBar, ScrollView, Image, TouchableOpacity, TextInput, Modal, ActivityIndicator } from 'react-native';
import styles from './styles';
import COLOR from '../../Config/Color';
import CountryPicker, { FlagButton } from 'react-native-country-picker-modal';
import auth from '@react-native-firebase/auth';
import OTPTextInput from 'react-native-otp-textinput';
import Toast from 'react-native-root-toast';
import ToastConfiq from '../../Config/ToastConfiq';
import { validateAll } from 'indicative/validator';
import { Icon } from 'native-base';
import API from '../../Config/API';
import { existuserService } from '../../Redux/Services/LoginServices';
const rules = {
    phone: 'required|number'
}
const messages = {
    required: (field) => `${field} is required`,
}
class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.unsubscribe = null;
        this.state = {
            loading: false,
            error: {},
            errors: null,
            phone: null,
            ccd1: 91,
            cca2: 'IN',
            checktop: false,
            otp: null,
            CheckingUser: false,
            Message: "Please Wait"
        };
    }
    componentDidMount() {
        this.unsubscribe = auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({loading: true,CheckingUser: true });
                //check phone number in database user.phoneNumber\
                let obj = {
                    mobile: user.phoneNumber,
                    fcmToken: "null"
                }
                fetch(API.BASE_URL + API.LOGIN, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(obj)
                }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            loading: false,
                            checktop: false,
                            phone: null,
                            otp: null
                        });
                        if (responseJson.status === true) {
                            this.setState({CheckingUser: false});
                            this.props.existuserService(responseJson.data);
                            this.props.navigation.reset({ routes: [{ name: 'User' }] })
                        } else {
                            Toast.show(responseJson.message, ToastConfiq);
                            auth().signOut();
                            this.setState({
                                formattedErrors: responseJson.message,
                                Message: responseJson.message
                            });
                        }
                    })
                    .catch((error) => {
                        Toast.show('Something went wrong.try again', ToastConfiq);
                        auth().signOut();
                        this.setState({
                            errors: JSON.stringify(error),
                            CheckingUser: false,
                            loading: false,
                            checktop: false,
                            phone: null,
                            otp: null
                        });
                    })
            }
        })
    }

    componentWillUnmount() {
        if (this.unsubscribe) this.unsubscribe();
    }

    login = (data) => {
        validateAll(data, rules, messages)
            .then((success) => {
                console.log('+' + success.ccd1 + success.phone)
                this.setState({ loading: true, error: {} });
                auth().signInWithPhoneNumber('+' + success.ccd1 + success.phone)
                    .then(confirmResult => {
                        this.setState({
                            confirmResult,
                            checktop: true,
                            loading: false,
                            errors: null
                        });
                    }).catch(error1 => {
                        Toast.show('Something went wrong.try again', ToastConfiq);
                        this.setState({ errors: error1.message, loading: false });
                    })
            })
            .catch((errors) => {
                const formattedErrors = {}
                errors.forEach(error => formattedErrors[error.field] = error.message);
                this.setState({ error: formattedErrors })
            })
    }

    verifyingService = () => {
        const { confirmResult, otp } = this.state;
        if (confirmResult && otp.length) {
            this.setState({ loading: true, errors: null });
            confirmResult.confirm(otp)
                .then(user => {
                    this.setState({ loading: false });
                }).catch(error3 => {
                    Toast.show('Something went wrong.try again', ToastConfiq);
                    this.setState({
                        errors: `otp verification error: ${error3.message}`,
                        loading: false
                    });
                })
        } else {
            this.setState({ errors: 'please enter otp' });
        }
    }

    resendotp = () => {
        Toast.show('Resending OTP.',ToastConfiq);
        auth().signInWithPhoneNumber('+' + this.state.ccd1 + this.state.phone)
          .then(confirmResult => {
            this.setState({
              confirmResult,
              checktop: true,
              loading: false,
              errors: null
            });
          }).catch(error => {
            Toast.show('Something went wrong.try again',ToastConfig);
            this.setState({ errors: error.message, loading: false });
          })
      }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.Container}>
                <StatusBar backgroundColor={COLOR.primary3} barStyle="dark-content" />
                {this.state.checktop === true
                    ? <View style={styles.box1}>
                        <View style={styles.box2}>
                            <Image style={styles.logo} resizeMode='center'
                                source={require('../../Image/splash.png')} />
                        </View>
                        <View style={styles.box3}>
                            <Text style={styles.Text1}>What’s the code?</Text>
                            <Text style={styles.Text2}>Enter the code sent to your number</Text>
                            <View>
                                <OTPTextInput
                                    inputCount={6}
                                    tintColor={COLOR.primary1}
                                    handleTextChange={(otp) => this.setState({ otp })}
                                    textInputStyle={styles.otpinput}
                                    ref={e => (this.otpInput = e)}
                                />
                            </View>
                            <Text style={styles.Text2}>Didn’t received any code?</Text>
                            <TouchableOpacity
                            onPress={()=>{this.resendotp()}}>
                                <Text style={styles.resendtxt}>Resend Code?</Text>
                            </TouchableOpacity>
                            {this.state.errors
                                ? <View style={styles.errorbox}>
                                    <Text style={styles.errorboxtxt}>{this.state.errors}.Try again</Text>
                                    <Icon style={styles.errorboxicon} name="error-outline" type="MaterialIcons" />
                                </View>
                                : null
                            }
                            <TouchableOpacity style={styles.nextbtn}
                                onPress={() => { this.state.loading ? null : this.verifyingService() }}>
                                {this.state.loading
                                    ? <ActivityIndicator size="small" color="#fff" style={styles.loading} />
                                    : <Text style={styles.nextbtntxt}>Next</Text>
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                    : <View style={styles.box1}>
                        <View style={styles.box2}>
                            <Image style={styles.logo} resizeMode='center'
                                source={require('../../Image/splash.png')} />
                        </View>
                        <View style={styles.box3}>
                            <Text style={styles.Text1}>Verify your Phone number</Text>
                            <Text style={styles.Text2}>We have sent you an SMS with a code to number</Text>
                            <View
                                style={[styles.inputBox, this.state.error['phone'] ? { borderColor: 'red' } : { borderColor: COLOR.Gray1 }]}>
                                <View style={styles.ccdbox}>
                                    <FlagButton withEmoji={true} countryCode={this.state.cca2} />
                                    <CountryPicker
                                        placeholder={<Text style={styles.phonecodetxt}>+{this.state.ccd1}</Text>}
                                        withCallingCode={true}
                                        withFlag={true}
                                        withFlagButton={true}
                                        withEmoji={true}
                                        onSelect={value => { this.setState({ ccd1: value.callingCode[0], cca2: value.cca2 }) }} />
                                </View>
                                <TextInput
                                    keyboardType={"number-pad"}
                                    style={styles.input}
                                    placeholder="Enter Phone Numer"
                                    onChangeText={(phone) => this.setState({ phone })} />
                            </View>
                            {this.state.error['phone'] && <Text style={styles.errorMessage}>*{this.state.error['phone']}</Text>}

                            {this.state.errors
                                ? <View style={styles.errorbox}>
                                    <Text style={styles.errorboxtxt}>{this.state.errors}.Try again</Text>
                                    <Icon style={styles.errorboxicon} name="error-outline" type="MaterialIcons" />
                                </View>
                                : null
                            }
                            <TouchableOpacity style={styles.nextbtn}
                                onPress={() => { this.state.loading ? null : this.login(this.state) }}>
                                {this.state.loading
                                    ? <ActivityIndicator size="small" color="#fff" style={styles.loading} />
                                    : <Text style={styles.nextbtntxt}>Next</Text>
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.CheckingUser}>
                    <StatusBar backgroundColor={COLOR.transparent} barStyle="dark-content" />
                    <View style={styles.checkusermodal}>
                        <View style={styles.loadingbox}>
                            {this.state.Message !== "Please Wait"
                                ? <Image style={styles.icons} resizeMode="stretch"
                                    source={require('../../Image/notfound.png')} />
                                : null}
                            {this.state.Message === "Please Wait" ? <ActivityIndicator size="large" color={COLOR.primary1} /> : null}
                            <Text style={styles.pleasewait}>{this.state.Message}</Text>
                            {this.state.Message !== "Please Wait"
                                ? <TouchableOpacity
                                    onPress={() => this.setState({ CheckingUser: false })}>
                                    <Text style={styles.closetxt}>Ok</Text>
                                </TouchableOpacity>
                                : null}
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        Login: state.Login
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        existuserService: (user) => {
            dispatch(existuserService(user));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);