import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flexGrow:1,
        backgroundColor:'#fff'
    },
    box1:{
        flex:1,
        flexDirection:'column'
    },
    box2:{
        backgroundColor:COLOR.primary3,
        flex:2,
        alignItems:'center',
        justifyContent:'center'
    },
    box3:{
        flex:1,
        backgroundColor:COLOR.White,
        justifyContent:'center',
        padding:20
    },
    Text1:{
        fontFamily:'OpenSans-Bold',
        fontSize:20,
        textAlign:'center',
    },
    Text2:{
        fontFamily:'OpenSans-Regular',
        textAlign:'center',
        marginTop:10,
        marginBottom:10,
        color:COLOR.Gray4,
        fontSize:14,
    },
    logo:{
        height:262,
        margin:20
    },
    nextbtn:{
        backgroundColor:COLOR.primary1,
        alignItems:'center',
        borderRadius:50,
        padding:10,
        top:20,
        marginBottom:20
    },
    nextbtntxt:{
        color:'#fff',
        fontSize:14,
        fontFamily:'OpenSans-Regular',
        margin:3
    },
    inputBox:{
        backgroundColor:COLOR.Gray1,
        borderRadius:50,
        flexDirection:'row',
        alignItems:'center',
        borderWidth:1
    },
    input:{
        flex:1,
        fontFamily:'OpenSans-Regular',
        marginLeft:10
    },
    ccdbox:{
        marginLeft:10,
        borderRightColor:COLOR.Gray4,
        borderRightWidth:1,
        paddingRight:10,
        flexDirection:'row',
        alignItems:'center',
    },
    phonecodetxt:{
        fontFamily:'OpenSans-Regular',
        marginLeft:10,
        fontSize:14
    },
    resendtxt:{
        fontFamily:'OpenSans-Bold',
        textAlign:'center',
        fontSize:14
    },
    otpinput:{
        backgroundColor:COLOR.Gray1,
        borderRadius:5,
        flexDirection:'row',
        alignItems:'center',
        height:50,
        width:40,
    },
    errorMessage:{
        color:'red',
        fontFamily:'OpenSans-Regular',
        fontSize:12,
        marginTop:5,
        marginLeft:10
    },
    errorbox:{
        flexDirection:'row',
        marginTop:30,
        borderRadius:5,
        padding:10,
        backgroundColor:'#f7d0d5'
    },
    errorboxtxt:{
        fontFamily:'OpenSans-Regular',
        color:'red',
        flex:1 
    },
    errorboxicon:{
        color:'red',
        fontSize:17
    },
    checkusermodal:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:COLOR.transparent
    },
    loadingbox:{
        backgroundColor:COLOR.White,
        padding:20,
        borderRadius:10,
        margin:20
    },
    pleasewait:{
        fontFamily:'OpenSans-SemiBold',
        color:COLOR.Gray3,
        marginTop:5 ,
        textAlign:'center'
    },
    loading:{
        margin:5
    },
    closetxt:{
        fontFamily:'OpenSans-SemiBold',
        color:COLOR.primary1,
        textAlign:'center' ,
        marginTop:20,
        fontSize:15
    },
    icons:{
        width:60,
        height:60,
        alignSelf:'center'
    }
})