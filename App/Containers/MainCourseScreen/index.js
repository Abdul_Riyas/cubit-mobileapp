import React, { Component } from 'react';
import { View,Text,StatusBar,ScrollView,FlatList} from 'react-native';
import COLOR from '../../Config/Color';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import CategoryItem from '../../Components/CategoryItem';
class MainCourseScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          title: '',
          headerStyle: styles.headerStyle,
          headerTintColor: COLOR.White,
          headerTitleStyle: styles.headerTittle,
          headerBackground:()=><LinearGradient colors={[COLOR.primary1, COLOR.primary1]} style={{ flex: 1 }}/>
        }
      }
      constructor(props) {
        super(props);
        this.state = {
          loading: true,
          errormsg:null,
          courses: ['BANK','PSC','SSE','CBSC']
        };
      }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.Container}>
              <StatusBar backgroundColor={COLOR.primary1} barStyle="light-content" />
              <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={{height:130}}/>
              <View style={styles.box}>
                <Text style={styles.heading}>Select course</Text>
                <FlatList
                style={{ flex: 1 }}
                initialNumToRender={10}
                maxToRenderPerBatch={10}
                data={this.state.courses}
                renderItem={({ item, index }) =>
                  <View style={styles.listitem}>
                    <CategoryItem
                      item={item}/>
                  </View>}
                keyExtractor={item => item}
                numColumns={2}
              />
              </View>
            </ScrollView>
        )
    }
}
export default MainCourseScreen;