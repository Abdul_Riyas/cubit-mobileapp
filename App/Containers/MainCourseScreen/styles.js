import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');
import COLOR from '../../Config/Color';
export default StyleSheet.create({
    Container: {
        flexGrow:1,
        backgroundColor:'#fff'
    },
    headerStyle:{

    },
    headerTittle:{
        fontFamily:'OpenSans-Regular',
        fontSize:16
    },
    box:{
        marginTop:-130,
        margin:5
    },
    heading:{
        textAlign:'center',
        color:'#fff',
        fontFamily:'OpenSans-Regular',
        fontSize:16,
        marginBottom:20
    },
    listitem:{
        flex:1,
        flexDirection:'column',
        margin:10
    }
})