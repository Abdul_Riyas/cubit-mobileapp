import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import { Icon} from 'native-base';
import CommonStyles from './CommonStyles';
import { withNavigation } from '@react-navigation/compat';
class HeaderNotification extends Component {
    render() {
        return (
            <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('NotificationScreen')}
            >
             <Icon name="md-notifications" type="Ionicons" style={CommonStyles.Noticon} />
          </TouchableOpacity>
        )
    }
}
export default withNavigation(HeaderNotification);