import * as React from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Icon, Card } from 'native-base';
import { withNavigation } from '@react-navigation/compat';
import CommonStyles from './CommonStyles';
class ErrorBox extends React.PureComponent {
    render() {
        return (
            <View style={CommonStyles.ErrorBox}>
            <Icon type="AntDesign" name="closecircle" style={CommonStyles.erroricon}/>
            <Text  style={CommonStyles.oops}>Oops,</Text>
              <Text style={CommonStyles.errortxt}>Something went wrong. Let’s try One more again.</Text>
              <TouchableOpacity style={CommonStyles.errorbtn} onPress={()=>this.props.refresh()}>
                <Text style={CommonStyles.errorbtntxt}>Try Again</Text>
              </TouchableOpacity>
          </View>
        );
    }
}
export default ErrorBox;
