import * as React from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Icon, Card } from 'native-base';
import { withNavigation } from '@react-navigation/compat';
import CommonStyles from './CommonStyles';
import {registrationSerivce} from '../Redux/Services/LoginServices';
class ExamItem extends React.Component {
    render() {
        return (
            <TouchableOpacity>
                <Card style={CommonStyles.CategoryItem}>
                    <Icon name="book" style={CommonStyles.CategoryItemIcon}/>
        <Text style={CommonStyles.CategoryItemtxt} numberOfLines={2}>Exams</Text>
                </Card>
            </TouchableOpacity>
        );
    }
}
function mapStateToProps(state) {
    return {
      Login: state.Login
    }
  }
const mapDispatchToProps = (dispatch) => {
    return {
        registrationSerivce: (data) => {
        dispatch(registrationSerivce(data));
      },
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(ExamItem);
