import * as React from 'react';
import { Text, View, TouchableOpacity, Image, Linking } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import CommonStyles from './CommonStyles';
import COLOR from '../Config/Color';
import LinearGradient from 'react-native-linear-gradient';
class ExperdBox extends React.PureComponent {
    render() {
        return (
            <View style={CommonStyles.Box}>
                <Image
                    style={CommonStyles.nodataimg}
                    source={require('../Image/exp.png')}
                    resizeMode={'stretch'}
                />
                <Text style={CommonStyles.trialtxt}>Your Account has expired</Text>
                <Text style={CommonStyles.trialtxt1}>please Contact Support Team.</Text>

                <TouchableOpacity
                    onPress={() => { Linking.openURL(`tel:+919447721923`) }}>
                    <LinearGradient colors={[COLOR.primary1, COLOR.primary2]} style={CommonStyles.trialbtn}>
                        <Text style={CommonStyles.trialbtntxt}>Contact</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}
export default withNavigation(ExperdBox);
