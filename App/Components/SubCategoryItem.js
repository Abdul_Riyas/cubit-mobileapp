import * as React from 'react';
import { connect } from 'react-redux';
import { Text,TouchableOpacity, Image } from 'react-native';
import { Icon, Card } from 'native-base';
import { withNavigation } from '@react-navigation/compat';
import CommonStyles from './CommonStyles';
import API from '../Config/API';
class SubCategoryItem extends React.Component {
  render() {
    return (
      <TouchableOpacity
       onPress={()=>this.props.navigation.navigate('VideoScreen',{item:this.props.item})}>
        <Card style={CommonStyles.SubCategoryItem}>
        <Image  source={{uri:API.IMAGE_PATH+this.props.item.image_url}} style={CommonStyles.caticon}/>
          <Text style={CommonStyles.CategoryItemtxt} numberOfLines={2}>{this.props.item.title}</Text>
        </Card>
      </TouchableOpacity>
    );
  }
}
function mapStateToProps(state) {
  return {
    Login: state.Login
  }
}
export default connect(mapStateToProps)(withNavigation(SubCategoryItem));
