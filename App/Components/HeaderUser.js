import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import { Icon} from 'native-base';
import CommonStyles from './CommonStyles';
import { withNavigation } from '@react-navigation/compat';
class HeaderUser extends React.PureComponent {
    render() {
        return (
            <TouchableOpacity 
            onPress={() => this.props.navigation.navigate('ProfileScreen')}
            >
            <Icon name="account" type="MaterialCommunityIcons" style={CommonStyles.usericon} />
          </TouchableOpacity>
        )
    }
}
export default withNavigation(HeaderUser);