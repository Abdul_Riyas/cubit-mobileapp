import * as React from 'react';
import { connect } from 'react-redux';
import { Text, View,TouchableOpacity,Image} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import CommonStyles from './CommonStyles';
class NodataBox extends React.PureComponent {
  render() {
    return (
      <View style={CommonStyles.Box}>
         <Image
              style={CommonStyles.nodataimg}
              source={require('../Image/nodata.png')}
              resizeMode={'stretch'}
            />
          <Text style={CommonStyles.trialtxt}>No data found.</Text>
      </View>
    );
  }
}
export default withNavigation(NodataBox);
