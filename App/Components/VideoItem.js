import * as React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { WebView } from 'react-native-webview';
import CommonStyles from './CommonStyles';
class VideoItem extends React.PureComponent {
  getVimeoPageURL(id) {
    return 'https://player.vimeo.com/video/'+id+'?transparent=0&title=0&byline=0&portrait=0&sidedock=0&controls=false&autoplay=false';
  }
  render() {
    return (
      <TouchableOpacity
      onPress={() =>{
        this.props.navigation.navigate('VideoPlayerScreen',{data:this.props.items})
    }}>
        <View style={{ backgroundColor: 'black',flex: 1}}>
          <WebView
            mediaPlaybackRequiresUserAction={true} 
            allowsFullscreenVideo={true}
            javaScriptEnabled={true}
            allowsInlineMediaPlayback={true}
            startInLoadingState = {true}
            allowsInlineMediaPlayback = {true}
            ref="webviewBridge"
            style={{
              minHeight:300,
              borderRadius: 10,
            }}
            source={{ uri: this.getVimeoPageURL(this.props.items.video_url) }}
            onError={error => console.error(error)}
          />
        </View>
        <View style={{ padding: 20, borderTopWidth: 1, borderColor: 'black', flexDirection: 'row' }}>
          <View>
            <Text style={{ fontFamily: 'OpenSans-SemiBold' }}>{this.props.items.video_title}</Text>
          </View>
          <View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
export default withNavigation(VideoItem);
