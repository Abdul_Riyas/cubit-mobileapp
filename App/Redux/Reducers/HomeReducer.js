import * as types from '../Actions/types';

const initialState = {
    loading: true,
    errormsg: false,
    banners: [],
    popular_videos: [],
    courses: []
}

const HomeReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.INTRO:
            return {
                ...state,
                intro: false
            }
        default:
            return state;
    }
}
export default HomeReducer;