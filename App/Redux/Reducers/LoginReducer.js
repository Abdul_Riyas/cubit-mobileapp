import * as types from '../Actions/types';

const initialState = {
    loading: false,
    errormsg: null,
    auth: false,
    token: null,
    firekey: null,
    userid: null,
    phonenumber: null,
    intro: true,
    trial: false,
    trialDate: null,
    paid: 0,
    User: {}
}

const LoginReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.INTRO:
            return {
                ...state,
                intro: false
            }

        case types.TRAIL:
            return {
                ...state,
                trial: true,
                auth: true,
                paid: 0,
                trialDate: action.payload
            }

        case types.UPDATE_PROFILE:
            return {
                ...state,
                loading: false,
                auth: true,
                User: action.payload
            }

        case types.SAVE_FIREKEY:
            return {
                ...state,
                loading: false,
                firekey: action.payload
            }

        case types.SAVE_TOKEN:
            return {
                ...state,
                loading: false,
                token: action.payload
            }

        case types.SAVE_USERID:
            return {
                ...state,
                loading: false,
                userid: action.payload
            }

        case types.SAVE_TRIALDATE:
            return {
                ...state,
                loading: false,
                trialDate: action.payload
            }

        case types.UPDATE_PAID:
            return {
                ...state,
                loading: false,
                paid: action.payload
            }

        case types.SAVE_NUMBER:
            return {
                ...state,
                loading: false,
                phonenumber: action.payload
            }

        case types.REGISTRATION_REQUEST:
            return {
                ...state,
                loading: true,
                errormsg: null,
            }

        case types.REGISTRATION_SUCCESS:
            return {
                ...state,
                loading: false,
                auth: true,
                paid: 1,
                trial: false,
                errormsg: null,
                User: action.payload
            }

        case types.REGISTRATION_FAILD:
            return {
                ...state,
                loading: false,
                errormsg: action.payload
            }

        case types.EXIST_USER:
            return {
                ...state,
                loading: false,
                auth: true,
                paid: 1,
                trial: false,
                errormsg: null,
                User: action.payload
            }

        case types.LOGOUT:
            return {
                ...state,
                loading: false,
                errormsg: null,
                auth: false,
                token: null,
                firekey: null,
                phonenumber: null,
                trial: false,
                trialDate: null,
                paid: 0,
                User: {}
            }

        default:
            return state;
    }
}
export default LoginReducer;