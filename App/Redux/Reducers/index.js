import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer';
const index = combineReducers({
    Login: LoginReducer,
})
export default index;
