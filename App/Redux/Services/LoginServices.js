import * as Action from '../Actions/LoginAction';
import * as ReduxNavigation from '../../Routes/ReduxNavigation';
import API from '../../Config/API';
import auth from '@react-native-firebase/auth';

export const introService = () => {
    return async dispatch => {
        dispatch(Action.intro());
        ReduxNavigation.navigate('LoginScreen');
    }
}

export const updateprofileService = (data) => {
    return async dispatch => {
        dispatch(Action.updateprofile(data));
    }
}

export const savefirekeyService = (firekey) => {
    return async dispatch => {
        dispatch(Action.savefirekey(firekey));
    }
}


export const existuserService = (data) => {
    return async dispatch => {
        dispatch(Action.existuser(data));
    }
}

export const logoutService = (token) => {
    return async dispatch => {
        fetch(API.BASE_URL + API.LOGOUT, {
            method: 'GET',
            headers: new Headers({
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            })
        })
            .then((response) => response.json())
            .then((json) => {
                auth().signOut();
                dispatch(Action.logout());
                ReduxNavigation.navigate('SplashScreen');
            })
            .catch((error) => {
                alert(JSON.stringify(error));
            });

    }
}

export const refreshuser = (tocken,userid) => {
    return async dispatch => {
        fetch(API.BASE_URL + API.REFRESH_USER +'/'+userid, {
            method: 'GET',
            headers: new Headers({
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer '+tocken
            })
        })
        .then((response) => response.json())
            .then((json) => {
                if(json.data){
                    let data = {
                        "access_token":tocken,
                        "user":json.data.user
                    }
                    dispatch(Action.existuser(data));
                }
              })
              .catch((error) => {
                alert(JSON.stringify(error)); 
              });
    }
}

