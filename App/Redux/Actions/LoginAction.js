import * as types from './types';

export const intro = () => ({
    type: types.INTRO
});

export const trailout = (date) => ({
    type: types.TRAIL,
    payload: date
});

export const updateprofile = (data) => ({
    type: types.UPDATE_PROFILE,
    payload: data
});

export const savefirekey = (firekey) => ({
    type: types.SAVE_FIREKEY,
    payload: firekey
});

export const savetoken = (token) => ({
    type: types.SAVE_TOKEN,
    payload: token
});

export const saveuserid = (userid) => ({
    type: types.SAVE_USERID,
    payload: userid
});

export const savetrialDate = (date) => ({
    type: types.SAVE_TRIALDATE,
    payload: date
});

export const updatePaidStatus = (data) => ({
    type: types.UPDATE_PAID,
    payload: data
});

export const savephonenumber = (phonenumber) => ({
    type: types.SAVE_NUMBER,
    payload: phonenumber
});

export const logout = () => ({
    type: types.LOGOUT
});

export const regisrationrequest = () => ({
    type: types.REGISTRATION_REQUEST
});

export const registrationSuccess = (data) => ({
    type: types.REGISTRATION_SUCCESS,
    payload: data
});

export const registrationfailure = (error) => ({
    type: types.REGISTRATION_FAILD,
    payload: error
});

export const existuser = (data) => ({
    type: types.EXIST_USER,
    payload: data
});









