import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import Reducer from '../Reducers';
import thunk from 'redux-thunk';
const persistConfig = {
    key: 'state',
    storage: AsyncStorage,
    //blacklist:['Login']

}
const persistedReducer = persistReducer(persistConfig, Reducer);
const Store = createStore(
    persistedReducer,
    applyMiddleware(thunk)
)
const PersistedStore = persistStore(Store);
export { Store, PersistedStore };