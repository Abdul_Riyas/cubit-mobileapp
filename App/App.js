import React from 'react';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import Routes from './Routes';
import { Store, PersistedStore } from './Redux/Store/Store';
import { PersistGate } from 'redux-persist/integration/react';
import { navigationRef } from './Routes/ReduxNavigation';
import { RootSiblingParent } from 'react-native-root-siblings';
function App() {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={PersistedStore}>
        <RootSiblingParent>
          <NavigationContainer ref={navigationRef}>
            <Routes />
          </NavigationContainer>
        </RootSiblingParent>
      </PersistGate>
    </Provider>
  );
};
export default App;

