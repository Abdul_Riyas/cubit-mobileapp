const COLOR = {
    primary1:'#2AACE0',
    primary2:'#155670',
    primary3:'#F2F9FF',
    black:'#000',
    White:'#fff',

    Gray1:'#F0F0F0', //Border
    Gray2:'#f0f2f5', //Background
    Gray3:'#373737', //Button
    Gray4:'#919191', //Tab Icons
    transparent:'rgba(52, 52, 52, 0.3)'
};
export default COLOR;