import Toast from 'react-native-root-toast';
const ToastConfiq = {
    duration: Toast.durations.SHORT,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    hideOnPress: true,
    backgroundColor:'#fff',
    textColor:'#000'
    
};
export default ToastConfiq;