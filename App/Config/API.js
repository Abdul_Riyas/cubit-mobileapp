const API = {
    BASE_URL:'http://cubit.optimisttechhub.com/api/v1/',
    IMAGE_PATH:'http://cubit.optimisttechhub.com/',
    FILE_PATH:'http://cubit.optimisttechhub.com/storage/video-documents/',
    LOGIN:'login',
    LOGOUT:'logout',
    REGISTER:'register',
    GET_HOME_DATA:'get-home-page',
    GET_CATEGORIES:'courses',
    GET_COURSE_VIDEO:'videos',
    GET_EXAMS:'exam-types',
    GET_MOCKTEST_LEVEL:'exam-categories',
    GET_EXAMS_LEVEL:'exams',
    GET_EXAMS_QUESTIONS:'exam',
    GET_MOCKTEST_QUESTIONS:'mock-test',
    GET_PAYMENT:'payment-details',
    GET_NOTIFICATION:'notifications',
    UPDATE_USER:'update-user',
    POST_FEEDBACK:'feedback',
    SUBMIT_EXAM:'submit-exam',
    REFRESH_USER:'user',
    CHECKSUMHASH:'http://msptalks.co.in/Checksum/generateChecksum.php'
};
export default API;

//Bearer 