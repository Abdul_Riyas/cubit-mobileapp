import * as React from 'react';
import { connect } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import styles from './styles';
import COLOR from '../Config/Color';
import { Icon } from 'native-base';
import { View, Text } from 'react-native';

import SplashScreen from '../Containers/SplashScreen';
import IntorScreen from '../Containers/IntorScreen';
import LoginScreen from '../Containers/LoginScreen';
import MainCourseScreen from '../Containers/MainCourseScreen';
import HomeScreen from '../Containers/HomeScreen';
import AboutScreen from '../Containers/AboutScreen';
import CategorieScreen from '../Containers/CategorieScreen';
import ExamScreen from '../Containers/ExamScreen';
import VideoScreen from '../Containers/VideoScreen';
import VideoPlayerScreen from '../Containers/VideoPlayerScreen';
import ProfileScreen from '../Containers/ProfileScreen';
import NotificationScreen from '../Containers/NotificationScreen';
import AppinfoScreen from '../Containers/AppinfoScreen';
import HelpScreen from '../Containers/HelpScreen';
import PrivacyScreen from '../Containers/PrivacyScreen';
import TermScreen from '../Containers/TermScreen';
import PopularVideoScreen from '../Containers/PopularVideoScreen';
import EditProfileScreen from '../Containers/EditProfileScreen';
import ChatScreen from '../Containers/ChatScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = () => {
    return <Stack.Navigator initialRouteName='HomeScreen'>
        <Stack.Screen
            name="HomeScreen"
            component={HomeScreen}
            options={HomeScreen.navigationOptions} />
    </Stack.Navigator>
}

const CategorieStack = () => {
    return <Stack.Navigator initialRouteName='CategorieScreen'>
        <Stack.Screen
            name="CategorieScreen"
            component={CategorieScreen}
            options={CategorieScreen.navigationOptions} />
    </Stack.Navigator>
}

const ExamStack = () => {
    return <Stack.Navigator initialRouteName='ExamScreen'>
        <Stack.Screen
            name="ExamScreen"
            component={ExamScreen}
            options={ExamScreen.navigationOptions} />
    </Stack.Navigator>
}

const AboutStack = () => {
    return <Stack.Navigator initialRouteName='AboutScreen'>
        <Stack.Screen
            name="AboutScreen"
            component={AboutScreen}
            options={AboutScreen.navigationOptions} />
    </Stack.Navigator>
}
const Tabs = () => {
    return (
        <Tab.Navigator initialRouteName="Home"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    let type;
                    if (route.name === 'Home') {
                        iconName = focused ? 'home' : 'home';
                        type = focused ? 'Entypo' : 'SimpleLineIcons';
                    }
                    else if (route.name === 'Categories') {
                        iconName = focused ? 'view-grid' : 'grid-large';
                        type = 'MaterialCommunityIcons';
                    }
                    else if (route.name === 'Exams') {
                        iconName = focused ? 'edit' : 'edit';
                        type = 'AntDesign';
                    }
                    else if (route.name === 'About') {
                        iconName = focused ? 'infocirlce' : 'infocirlceo';
                        type = 'AntDesign';
                    }
                    return <Icon type={type} name={iconName} style={{ color: color, fontSize: 23, marginTop: 5 }} />;
                },
            })}
            tabBarOptions={{
                tabStyle: styles.tabStyle,
                style: styles.tabcommonstyle,
                activeTintColor: COLOR.primary1,
                inactiveTintColor: COLOR.Gray4,
                fontFamily: 'OpenSans-Regular',
            }}>
            <Tab.Screen name="Home" component={HomeStack} />
            <Tab.Screen name="Categories" component={CategorieStack} />
            <Tab.Screen name="Exams" component={ExamStack} />
            <Tab.Screen name="About" component={AboutStack} />
        </Tab.Navigator>
    )
}
const MainRoute = (props) => {
    return (
        <Stack.Navigator initialRouteName='SplashScreen' screenOptions={{ animationEnabled: false }}>
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="IntorScreen"
                component={IntorScreen}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="LoginScreen"
                component={LoginScreen}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="MainCourseScreen"
                component={MainCourseScreen}
                options={MainCourseScreen.navigationOptions} />
            <Stack.Screen
                name="User"
                component={Tabs}
                options={{ headerShown: false }} />
            <Stack.Screen
                name="VideoScreen"
                component={VideoScreen}
                options={VideoScreen.navigationOptions} />
            <Stack.Screen
                name="VideoPlayerScreen"
                component={VideoPlayerScreen}
                options={VideoPlayerScreen.navigationOptions} />
            <Stack.Screen
                name="ProfileScreen"
                component={ProfileScreen}
                options={ProfileScreen.navigationOptions} />
            <Stack.Screen
                name="NotificationScreen"
                component={NotificationScreen}
                options={NotificationScreen.navigationOptions} />
            <Stack.Screen
                name="AppinfoScreen"
                component={AppinfoScreen}
                options={AppinfoScreen.navigationOptions} />
            <Stack.Screen
                name="HelpScreen"
                component={HelpScreen}
                options={HelpScreen.navigationOptions} />
            <Stack.Screen
                name="PrivacyScreen"
                component={PrivacyScreen}
                options={PrivacyScreen.navigationOptions} />
            <Stack.Screen
                name="TermScreen"
                component={TermScreen}
                options={TermScreen.navigationOptions} />
            <Stack.Screen
                name="PopularVideoScreen"
                component={PopularVideoScreen}
                options={PopularVideoScreen.navigationOptions} />
            <Stack.Screen
                name="EditProfileScreen"
                component={EditProfileScreen}
                options={EditProfileScreen.navigationOptions} />
            <Stack.Screen
                name="ChatScreen"
                component={ChatScreen}
                options={ChatScreen.navigationOptions} />
        </Stack.Navigator>
    )
}
export default MainRoute;