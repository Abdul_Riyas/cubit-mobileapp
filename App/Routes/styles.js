import { StyleSheet, Dimensions } from 'react-native';
import COLOR from '../Config/Color';
const win = Dimensions.get('window');
export default StyleSheet.create({
    headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.primary,
        borderBottomWidth: .9,
        backgroundColor: COLOR.primary,
    },
    headerStyle2: {
        elevation: 0,
        shadowOpacity: 0,
        borderBottomColor:COLOR.White,
        borderBottomWidth:0,
        backgroundColor: COLOR.White,
    },
    headerTittle:{
        fontFamily:'HelveticaNeue Medium', 
        fontSize:18
    },
    tabStyle: {
        justifyContent: "center",
        elevation: 0,
        shadowOpacity: 0,
        borderTopColor:COLOR.White,
        borderTopWidth:0,
        marginBottom:5,
        marginTop:5
    },
    tabcommonstyle: {
        backgroundColor: "#fff",
        overflow: "hidden",
        elevation:0
    },
})